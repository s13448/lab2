package repository;

import entities.BaseEntity;

import java.sql.SQLException;
import java.util.List;

public interface Repository<TEntity> extends BaseEntity {

    List<TEntity> getAll();
    TEntity getById(int id);

    void add(TEntity entity);

    void update(TEntity entity) throws SQLException;

    void delete(TEntity entity);

    void createTable();
}
