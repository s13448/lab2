package repository;

import entities.BaseEntity;
import entities.Episode;
import entities.Season;
import mappers.ResultSetMapper;
import uow.UnitOfWork;

import java.sql.*;
import java.util.List;

public class SeasonRepository extends RepositoryBase<Season> {

    protected SeasonRepository(Connection connection, ResultSetMapper<Season> mapper, UnitOfWork uow) throws SQLException {
        super(connection, mapper, uow);
    }

    @Override
    protected void setupUpdate(Season season) throws SQLException {
        update.setString(1, season.getName());
        update.setInt(2, season.getSeasonNumber());
        update.setInt(3, season.getYearOfRelease());
        update.setArray(4, (Array) season.getEpisodes());
    }



    @Override
    protected void setupInsert(Season season) throws SQLException {
        insert.setString(1, season.getName());
        insert.setInt(2, season.getSeasonNumber());
        insert.setInt(3, season.getYearOfRelease());
        insert.setArray(4, (Array) season.getEpisodes());
        insert.setInt(4, season.getId());
    }


    @Override
    protected void setupDelete(Season season) throws SQLException {
        delete.setInt(1, season.getId());
    }

    @Override
    protected String tableName() {
        return "season";
    }

                //DO REPOSITORY WYGENEROWAC QUERY DO TWORZENIA TABELI Z FK I ITD

    @Override
    protected String createTableSql() {
        return  "CREATE TABLE season("
                + "id INT GENERATED BY DEFAULT AS IDENTITY AUTO INCREMENT,"
                + "name VARCHAR(20),"
                + "season_number INT,"
                + "year_of_release INT"
                + ")";
    }
    @Override
    protected String updateSql() {
        return "UPDATE season SET name=?, season_number=?,year_of_release = ? WHERE id=?" ;
    }

    @Override
    protected String insertSql() {
        return "INSERT INTO season(name,season_number,year_of_release) VALUES (?,?,?) WHERE id=?";
    }

    @Override
    protected String deleteSql() {
        return super.deleteSql();
    }

    @Override
    public int getId() {
        return 0;
    }

    @Override
    public void persistAdd(BaseEntity baseEntity) {

    }

    @Override
    public void persistUpdate(BaseEntity baseEntity) {

    }

    @Override
    public void persistDelete(BaseEntity baseEntity) {

    }
}
