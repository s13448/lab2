package repository;

import entities.*;
import mappers.*;
import uow.UnitOfWork;

import java.sql.Connection;
import java.sql.SQLException;

public class JdbcRepositoryCatalog implements RepositoryCatalog {

    Connection connection;
    UnitOfWork unitOfWork;

    public JdbcRepositoryCatalog(Connection connection, UnitOfWork uow) {
        this.connection = connection;
        this.unitOfWork = unitOfWork;
    }

    @Override
    public Repository<Actor> actors() {
        try{
            return new ActorRepository(connection, new ActorMapper(), this.unitOfWork);
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Repository<Director> directors() {
        try{
            return new DirectorRepository(connection, new DirecorMapper(), this.unitOfWork);
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Repository<Episode> episodes() {
        try{
            return new EpisodeRepository(connection, new EpisodeMapper(), this.unitOfWork);
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Repository<Season> seasons() {
        try{
            return new SeasonRepository(connection,new SeasonMapper(),this.unitOfWork);
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Repository<TvSeries> tvSeries() {
        try{
            return new TvSeriesRepository(connection, new TvSeriesMapper(),this.unitOfWork);
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void saveChanges() { this.unitOfWork.saveChanges();
    }

    @Override
    public void rollback() { this.unitOfWork.rollback();
    }
}
