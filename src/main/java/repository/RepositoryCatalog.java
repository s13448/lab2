package repository;

import entities.*;

/**
 * Created by Marcin on 2018-02-02.
 */
public interface RepositoryCatalog {
    Repository<Actor> actors ();
    Repository<Director> directors();
    Repository<Episode> episodes();
    Repository<Season> seasons ();
    Repository<TvSeries> tvSeries();

    void saveChanges();
    void rollback();
}
