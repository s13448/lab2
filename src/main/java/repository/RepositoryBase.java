package repository;

import entities.BaseEntity;
import mappers.ResultSetMapper;
import uow.Entity;
import uow.UnitOfWork;
import uow.UnitOfWorkRepository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public abstract class RepositoryBase<TEntity extends BaseEntity> implements Repository<TEntity>, UnitOfWorkRepository {
    protected Connection connection;
    protected Statement createTable;
    protected PreparedStatement insert;
    protected PreparedStatement selectAll;
    protected PreparedStatement getByIdQuery;
    protected PreparedStatement update;
    protected PreparedStatement delete;

    ResultSetMapper<TEntity> mapper;
    UnitOfWork uow;

    protected RepositoryBase(Connection connection, ResultSetMapper<TEntity> mapper, UnitOfWork uow) throws SQLException {
        this.mapper = mapper;
        this.connection = connection;
        this.uow = uow;
        createTable = connection.createStatement();
        createTable();
        connection.commit();
        insert = connection.prepareStatement(insertSql());
        update = connection.prepareStatement(updateSql());
        delete = connection.prepareStatement(deleteSql());
        selectAll = connection.prepareStatement(selectAllSql());
        getByIdQuery = connection.prepareStatement(getByIdQuery());
    }
    public List<TEntity> getAll(){
        List<TEntity> result = new ArrayList<TEntity>();
        try {
            ResultSet rs = selectAll.executeQuery();
            while(rs.next()){
                result.add(mapper.map(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public TEntity getById(int id){
        try {
            getByIdQuery.setInt(1, id);
            ResultSet rs = getByIdQuery.executeQuery();
            while(rs.next()){
                return mapper.map(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void createTable(){
        try {

            ResultSet rs = connection.getMetaData().getTables(null, null, null, null);
            boolean tableExists = false;
            while(rs.next()){
                if(rs.getString("TABLE_NAME").equalsIgnoreCase(tableName())){
                    tableExists=true;
                    break;
                }
            }
            if(!tableExists)
                createTable.executeUpdate(createTableSql());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected String selectAllSql() {
        return "SELECT * FROM " + tableName();
    }

    protected String getByIdQuery() {
        return "SELECT * FROM " + tableName() + " WHERE id = ?";
    }

    protected String deleteSql() {
        return "DELETE FROM "
                + tableName()
                + " WHERE id=?";
    }
    public void persistAdd(Entity entity){
        try{
            setupInsert((TEntity) entity.getEntity());
            insert.executeUpdate();
        }catch(SQLException ex){
            ex.printStackTrace();
        }
    }

    public void persistUpdate(Entity entity) {
        try{
            setupUpdate((TEntity) entity.getEntity());
            update.executeUpdate();
        }catch(SQLException ex){
            ex.printStackTrace();
        }
    }

    public void persistDelete(Entity entity) {
        try{
            setupDelete((TEntity) entity.getEntity());
            delete.executeUpdate();
        }catch(SQLException ex){
            ex.printStackTrace();
        }
    }

    public void add(TEntity entity) {
        Entity et = new Entity();
        et.setEntity(entity);
        et.setRepository(this);
        this.uow.markAsNew(et);
    }

    public void update(TEntity entity) throws SQLException {
        Entity et = new Entity();
        et.setEntity(entity);
        et.setRepository(this);
        this.uow.markAsChanged(et);
    }

    public void delete(TEntity entity) {
        Entity et = new Entity();
        et.setEntity(entity);
        et.setRepository(this);
        this.uow.markAsDeleted(et);
    }

    protected abstract void setupUpdate(TEntity entity) throws SQLException;
    protected abstract void setupInsert(TEntity entity) throws SQLException;
    protected abstract void setupDelete(TEntity entity) throws SQLException;
    protected abstract String tableName();
    protected abstract String createTableSql();
    protected abstract String updateSql();
    protected abstract String insertSql();
}
