package uow;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class JdbcUnitOfWork implements UnitOfWork {
    private Connection connection;
    private List<Entity> entityList = new ArrayList<>();

    public JdbcUnitOfWork(Connection connection) throws SQLException {
        this.connection = connection;
        this.connection.setAutoCommit(false);
    }

    public void markAsNew(Entity entity) {
        entity.setState(EntityState.NEW);
        entityList.add(entity);
    }

    public void markAsDeleted(Entity entity) {
        entity.setState(EntityState.DELETED);
        entityList.add(entity);
    }

    public void markAsChanged(Entity entity) {
        entity.setState(EntityState.CHANGED);
        entityList.add(entity);
    }

    public void saveChanges() {
        entityList.forEach(Entity::persistChange);
        try {
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        entityList.clear();
    }

    public void rollback() {
        entityList.clear();
    }
}

