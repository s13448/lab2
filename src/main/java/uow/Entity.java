package uow;

import com.sun.org.apache.xerces.internal.impl.validation.EntityState;
import entities.BaseEntity;

import static uow.EntityState.CHANGED;
import static uow.EntityState.NEW;


public class Entity implements BaseEntity {
    private BaseEntity entity;
    private UnitOfWorkRepository repository;
    private EntityState state;

    public BaseEntity getEntity() {
        return entity;
    }

    public void setEntity(BaseEntity entity) {
        this.entity = entity;
    }

    public UnitOfWorkRepository getRepository() {
        return repository;
    }

    public void setRepository(UnitOfWorkRepository repository) {
        this.repository = repository;
    }

    public EntityState getState() {
        return state;
    }

    public void setState(EntityState state) {
        this.state = state;
    }

    public void persistChange() {
        switch (state) {
            case NEW:
                repository.persistAdd(this);
                break;
            case CHANGED:
                repository.persistUpdate( this);
                break;
            case DELETED:
                repository.persistDelete( this);
                break;
            default:
                break;
        }
    }

    @Override
    public int getId() {
        return 0;
    }
}
