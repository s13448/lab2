package uow;

public interface UnitOfWork {
    void markAsNew(Entity entity);
    void markAsDeleted(Entity entity);
    void markAsChanged(Entity entity);
    void saveChanges();
    void rollback();
}
