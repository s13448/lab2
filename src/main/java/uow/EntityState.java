package uow;


public enum EntityState {
    NEW, DELETED, CHANGED, UNCHANGED
}
