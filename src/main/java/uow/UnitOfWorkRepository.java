package uow;

import entities.BaseEntity;


public interface UnitOfWorkRepository {
    void persistAdd(BaseEntity baseEntity);
    void persistUpdate(BaseEntity baseEntity);
    void persistDelete(BaseEntity baseEntity);
}
