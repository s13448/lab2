package dbFactory;

import repository.JdbcRepositoryCatalog;
import repository.RepositoryCatalog;
import uow.JdbcUnitOfWork;
import uow.UnitOfWork;

import java.sql.Connection;
import java.sql.DriverManager;

public class JdbcCatalogFactory implements DbCatalogFactory {
    @Override
    public RepositoryCatalog initConnection() {

        String url = "jdbc:hsqldb:hsql://localhost/workdb";

        try{
            Connection connection = DriverManager.getConnection(url);
            UnitOfWork uow = new JdbcUnitOfWork(connection);
            return new JdbcRepositoryCatalog(connection, uow);
        }catch (Exception e){
            e.printStackTrace();
        return null;
        }


    }
}
