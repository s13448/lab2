package dbFactory;

import repository.RepositoryCatalog;

public interface DbCatalogFactory {
    RepositoryCatalog initConnection();
}
