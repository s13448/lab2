import dbFactory.JdbcCatalogFactory;
import entities.Actor;
import entities.Episode;
import entities.Season;
import repository.RepositoryCatalog;
import service.EpisodeService;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        RepositoryCatalog entityFreamwork = new JdbcCatalogFactory().initConnection();


        Episode episode1 = new Episode(1, "Episode 1", 1, 1);
        entityFreamwork.episodes().add(episode1);
        Episode episode2 = new Episode(2, "Episode 2", 2, 2);
        entityFreamwork.episodes().add(episode2);
        Episode episode3 = new Episode(3, "Epsiode 3", 3, 3);
        entityFreamwork.episodes().add(episode3);
    List<Episode> episodeList =Arrays.asList(episode1,episode2,episode3);

        Season season = new Season(1, "Season 1", 1, 2015,episodeList) ;
        entityFreamwork.seasons().add(season);


        Actor actor1 = new Actor(1,"Brad Pitt", "1.01.1969","Holywood star");
        entityFreamwork.actors().add(actor1);
        Actor actor2 = new Actor(2,"Angelina Jolie", "1.21.1972","Holywood star, Brad Pitt's wife");
        entityFreamwork.actors().add(actor2);
        Actor actor3 = new Actor(3,"Jason Statham", "25.03.1973","Best know from transporter series");
        entityFreamwork.actors().add(actor3);

        entityFreamwork.saveChanges();

        List<Actor> actorList = entityFreamwork.actors().getAll();
        for(Actor e : actorList){
            System.out.println(e.getId() + "\t"
                    + e.getName() + "\t"
                    + e.getDateOfBirth());
        }

        List<Episode> episodes = entityFreamwork.episodes().getAll();
        for(Episode e : episodes) {
            System.out.println(e.getId() + "\t"
                    +e.getName() + "\t"
                    +e.getEpisodeNumber() + "\t"
                    +e.getSeasonNumber());
        }
    }
}