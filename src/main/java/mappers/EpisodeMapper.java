package mappers;

import entities.Episode;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Marcin on 2018-02-02.
 */
public class EpisodeMapper implements ResultSetMapper<Episode> {
    @Override
    public Episode map(ResultSet rs) throws SQLException {
        Episode episode = new Episode();
        episode.setId(rs.getInt("id"));
        episode.setName(rs.getString("name"));
        episode.setEpisodeNumber(rs.getInt("episodeNumber"));
        episode.setSeasonNumber(rs.getInt("seasonNumber"));
        return episode;
    }
}
