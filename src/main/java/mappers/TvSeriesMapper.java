package mappers;

import entities.Season;
import entities.TvSeries;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Marcin on 2018-02-02.
 */
public class TvSeriesMapper implements ResultSetMapper<TvSeries> {
    @Override
    public TvSeries map(ResultSet rs) throws SQLException {
        TvSeries tvSeries = new TvSeries();
        tvSeries.setId(rs.getInt("id"));
        tvSeries.setName(rs.getString("name"));
        tvSeries.setSeasons((List<Season>) rs.getArray("seasons"));
        return tvSeries;
    }
}
