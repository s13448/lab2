package mappers;


import entities.Director;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DirecorMapper implements ResultSetMapper<Director> {
    @Override
    public Director map(ResultSet rs) throws SQLException {
        Director director = new Director();
        director.setId(rs.getInt("id"));
        director.setName(rs.getString("name"));
        director.setBiography(rs.getString("biography"));
        director.setDateOfBirth(rs.getDate("dateOfBirth"));
        return director;
    }
}
