package mappers;

import entities.Actor;

import java.sql.ResultSet;
import java.sql.SQLException;


public class ActorMapper implements ResultSetMapper<Actor> {


    @Override
    public Actor map(ResultSet rs) throws SQLException {
        Actor actor = new Actor();
        actor.setId(rs.getInt("id"));
        actor.setName(rs.getString("name"));
        actor.setDateOfBirth(rs.getDate("dateOfBirth"));
        actor.setBiography(rs.getString("biography"));
        return actor;
    }
}
