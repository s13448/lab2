package mappers;

import entities.Episode;
import entities.Season;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;


public class SeasonMapper implements ResultSetMapper<Season> {
    @Override
    public Season map(ResultSet rs) throws SQLException {
        Season season = new Season();
        season.setId(rs.getInt("id"));
        season.setName(rs.getString("name"));
        season.setYearOfRelease(rs.getInt("yearOfRelease"));
        season.setEpisodes((List<Episode>) rs.getArray("episodes"));
        return null;
    }
}
