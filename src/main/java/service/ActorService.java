package service;

import entities.Actor;


import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ActorService {

    private Connection connection;
    private String url = "jdbc:hsqldb:hsql://localhost/workdb";
    private String createTable = "CREATE TABLE Actor (id bigint GENERATED BY DEFAULT AS IDENTITY, name varchar(20),date_of_birth datetime,biography varchar(500)";

    private PreparedStatement addActor;
    private PreparedStatement getAllActors;
    private PreparedStatement updateActor;
    private PreparedStatement deleteActors;

    private Statement statement;

    public ActorService() {
        try {
            connection = DriverManager.getConnection(url);
            statement = connection.createStatement();

            ResultSet rs = connection.getMetaData().getTables(null, null, null,
                    null);
            boolean tableExists = false;
            while (rs.next()) {
                if ("Actor".equalsIgnoreCase(rs.getString("TABLE_NAME"))) {
                    tableExists = true;
                    break;
                }
            }
            if (!tableExists)
                statement.executeUpdate(createTable);

            addActor = connection
                    .prepareStatement("INSERT INTO Actor (name, date_of_birth, biography) VALUES (?,?,?)");
            deleteActors = connection
                    .prepareStatement("DELETE FROM Actor WHERE id=(?)");
            getAllActors = (PreparedStatement) connection
                    .prepareStatement("SELECT  name FROM Actor");
            updateActor= (PreparedStatement) connection
                    .prepareStatement("UPDATE Actor SET name = (?), date_of_birth = (?), biography = (?)  WHERE id = (?)");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    Connection getConnection() {
        return connection;
    }


    public int addActor(Actor actor) {
        int count = 0;
        try {
            addActor.setString(1, actor.getName());
            addActor.setDate(2, (Date) actor.getDateOfBirth());
            addActor.setString(3, actor.getBiography());

            count = addActor.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return 0;
    }

    public List<Actor> getAllActors() {
        List<Actor> actors = new ArrayList<Actor>();
        try {
            ResultSet rs = getAllActors.executeQuery();

            while (rs.next()) {
                Actor e = new Actor(rs.getLong(0));
                e.setName(rs.getString(1));
                e.setBiography(rs.getString(2));
                e.setDateOfBirth(rs.getDate(3));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return actors;
    }
    public int updateActor (Actor actor) {
        int count = 0;
        try {
            updateActor.setString(1, actor.getName());
            updateActor.setString(2, actor.getBiography());
            updateActor.setDate(3, (Date) actor.getDateOfBirth());

            count = updateActor.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return count;
    }
    public int deleteActors(Actor actor) {
        int count = 0;
        try {
            deleteActors.setLong(1, actor.getId());

            count = deleteActors.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return count;
    }
}
