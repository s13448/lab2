package entities;

import java.util.Date;

public class Actor implements BaseEntity {
    private int id;
    private String name;
    private String dateOfBirth;
    private String biography;

    public Actor() {

    }

    public Actor(int id, String name, String dateOfBirth, String biography) {
        this.id = id;
        this.name = name;
        this.dateOfBirth = dateOfBirth;
        this.biography = biography;
    }

    @Override
    public int getId() { return id;
    }

    public void setId(int id) { this.id = id; }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getBiography() {
        return biography;
    }

    public void setBiography(String biography) {
        this.biography = biography;
    }

}
