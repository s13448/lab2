package entities;

import java.util.Date;

public class Director implements BaseEntity {


    private int id;
    private String name;
    private Date dateOfBirth;
    private String biography;

    public void setId(int id) { this.id = id; }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getBiography() {
        return biography;
    }

    public void setBiography(String biography) {
        this.biography = biography;
    }


    @Override
    public int getId() {
        return 0;
    }
}
