package entities;

import java.util.List;

public class TvSeries implements BaseEntity {
    private String name;
    private List<Season> seasons;
    private int id;


    public void setId(int id) { this.id = id; }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Season> getSeasons() {
        return seasons;
    }

    public void setSeasons(List<Season> seasons) {
        this.seasons = seasons;
    }


    @Override
    public int getId() {
        return 0;
    }
}
