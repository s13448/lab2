package entities;

import java.util.List;

public class Season implements BaseEntity {

    private int id;
    private String name;
    private int seasonNumber;
    private int yearOfRelease;
    private List<Episode> episodes;

    public Season(int id, String name, int seasonNumber, int yearOfRelease, List<Episode> episodes) {
        this.id = id;
        this.name = name;
        this.seasonNumber = seasonNumber;
        this.yearOfRelease = yearOfRelease;
        this.episodes = episodes;
    }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }
    public void setId(int id) { this.id = id; }
    public int getSeasonNumber() {
        return seasonNumber;
    }

    public void setSeasonNumber(int seasonNumber) {
        this.seasonNumber = seasonNumber;
    }

    public int getYearOfRelease() {
        return yearOfRelease;
    }

    public void setYearOfRelease(int yearOfRelease) {
        this.yearOfRelease = yearOfRelease;
    }

    public List<Episode> getEpisodes() {
        return episodes;
    }

    public void setEpisodes(List<Episode> episodes) {
        this.episodes = episodes;
    }



    public int getId() {
        return this.id;
    }
}
