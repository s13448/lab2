package entities;

public class Episode implements BaseEntity {

    private int id;
    private String name;
    private int episodeNumber;
    private int seasonNumber;

    public Episode(int id, String name, int episodeNumber, int seasonNumber) {
        this.id = id;
        this.name = name;
        this.episodeNumber = episodeNumber;
        this.seasonNumber = seasonNumber;
    }

    public void setId(int id) { this.id = id; }
    public int getSeasonNumber() {
        return seasonNumber;
    }

    public void setSeasonNumber(int seasonNumber) {
        this.seasonNumber = seasonNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEpisodeNumber(int episodeNumber) {
        this.episodeNumber = episodeNumber;
    }

    public int getEpisodeNumber() {
        return episodeNumber;
    }


    @Override
    public int getId() {
        return 0;
    }
}
