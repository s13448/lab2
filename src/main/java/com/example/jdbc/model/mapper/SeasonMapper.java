package com.example.jdbc.model.mapper;

import entities.BaseEntity;
import entities.Episode;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class SeasonMapper<T extends BaseEntity>{

    private Map<Long, T> loadedMap = new HashMap<>();
    protected Connection connection;


    abstract protected String findStatement();
    abstract protected String findAllEpisodes();
    abstract protected String insertStatement();
    abstract protected String updateStatement();
    abstract protected String removeStatement();

    abstract protected T doLoad(ResultSet rs) throws SQLException;
    abstract protected void parametrizeInsertStatement(PreparedStatement statement, T entity) throws SQLException;
    abstract protected void parametrizeUpdateStatement(PreparedStatement statement, T entity) throws SQLException;

    ////////////////DO REFAKTORU
    protected SeasonMapper(Connection connection) { this.connection = connection; }

    public T find(Long id) {
        T result = loadedMap.get(id);
        if (result != null) {
            return result;
        }
        PreparedStatement findStatement = null;
        try{
            findStatement = connection.prepareStatement(findStatement());
            findStatement.setLong(1,id.longValue());
            ResultSet rs = findStatement.executeQuery();
            rs.next();
            result = load(rs);
            return result;
        }catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public List<T> findAllEpisodes(Episode episode) {
        List<T> results = new ArrayList<>();
        PreparedStatement findAllEpisodes = null;
        try{
            findAllEpisodes = connection.prepareStatement(findAllEpisodes());
            findAllEpisodes.setLong(1,episode.getId());
            ResultSet rs = findAllEpisodes.executeQuery();

            while(rs.next()){
                T result = load(rs);
                results.add(result);
            }
            return results;
        }catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    public void add(T entity) {
        PreparedStatement addStatment = null;
        try{
            addStatment = connection.prepareStatement(insertStatement());

            parametrizeInsertStatement(addStatment, entity);

            addStatment.executeQuery();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    public void update(T entity){
        PreparedStatement updateStatement = null;
        try{
            updateStatement = connection.prepareStatement(updateStatement());

            parametrizeUpdateStatement(updateStatement, entity);

            updateStatement.executeQuery();
        }catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    public void remove(Long id){
        PreparedStatement removeStatement = null;
        try{
            removeStatement = connection.prepareStatement(removeStatement());
            removeStatement.setLong(1,id);

            removeStatement.executeQuery();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    private T load(ResultSet rs) throws SQLException{
        Long id = rs.getLong(1);
        if(loadedMap.containsKey(id)){
            return loadedMap.get(id);
        }
        T result = doLoad(rs);

        loadedMap.put(id, result);
        return result;
    }
}