package com.example.jdbc.model.mapper;

import entities.BaseEntity;
import entities.Episode;
import entities.Season;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public  class EpisodeMapper extends SeasonMapper {

    private static final String COLUMNS = "id, name, episodeNumber, duration";
    private static final String SELECT_EP_NAME = "SELECT" + COLUMNS + "FROM episode WHERE name=? OFFSET ? LIMIT ?";
    private static final String SELECT_EP_NUMBER = "SELECT" + COLUMNS +
            "FROM episode WHERE episodeNumber =? OFFSET ? LIMIT ?";
    private static final String INSERT_EP = "INSERT into epsiode(name, episodeNumber, duration) VALUES(?,?,?)";
    private static final String UPDATE_EP = "UPDATE episode SET(name, episodeNumber, duration) VALUES(?,?,?)";
    private static final String DELETE_EP = "DELETE episode WHERE id(?)";




    public EpisodeMapper(Connection connection) {
        super(connection);
    }

    public List<Episode> epName(String name, Season season){

        List<Episode> result = new ArrayList<>();
        PreparedStatement selectByName;
        try{
            selectByName = connection.prepareStatement(SELECT_EP_NAME);
            selectByName.setString(1, name);

            ResultSet rs = selectByName.executeQuery();
            while(rs.next()) {
                Episode episode = (Episode) doLoad(rs);
                result.add(episode);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
    @Override
    protected String findStatement() { return null; }

    @Override
    protected String findAllEpisodes() { return null; }

    @Override
    protected String insertStatement() {
        return INSERT_EP;
    }

    @Override
    protected String updateStatement() {
        return UPDATE_EP;
    }

    @Override
    protected String removeStatement() {
        return DELETE_EP;
    }

    @Override
    protected BaseEntity doLoad(ResultSet rs) throws SQLException {
        return null;
    }

    @Override
    protected void parametrizeInsertStatement(PreparedStatement statement, BaseEntity baseEntity) throws SQLException {
        statement.setString(1,
    }

    @Override
    protected void parametrizeUpdateStatement(PreparedStatement statement, BaseEntity baseEntity) throws SQLException {
        parametrizeInsertStatement(statement, baseEntity);
        statement.setLong(3, baseEntity.getId());
    }
}